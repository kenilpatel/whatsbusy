''' create a graph data structure to represent node and their connection'''


class Graph():

    def __init__(self, graph_string):
        ''' keeps track of inbound links of node '''
        self.inbound_links = {}
        ''' keeps track of outbound link of node '''
        self.outbound_links = {}
        ''' keeps track of all nodes present in graph '''
        self.nodes = []
        ''' we are passing the string as graph so we will call parse_graph
        method to process it '''
        self.parse_graph(graph_string)

    ''' this method is used to add edge in a graph '''

    def add_edge(self, source, destination):
        source = str(source)
        destination = str(destination)

        ''' add source and destination to list of nodes if they are not
        currently there '''
        if(source not in self.nodes):
            self.nodes.append(source)
        if(destination not in self.nodes):
            self.nodes.append(destination)

        ''' if source is present as key in outbound_link dictionary
        then just retrive the current list and add destination node to it'''
        if(source not in self.outbound_links.keys()):
            self.outbound_links[source] = [destination]
        else:
            self.outbound_links[source] = \
                self.outbound_links[source] + [destination]

        ''' if destination is present as key in inbound_links dictionary
        then just retrive the current list and add source node to it'''
        if(destination not in self.inbound_links.keys()):
            self.inbound_links[destination] = [source]
        else:
            self.inbound_links[destination] = \
                self.inbound_links[destination] + [source]

    ''' this method prints a graph  '''

    def print_graph(self):
        ''' fetch the list of nodes '''
        for node in self.nodes:
            print("connection of ", node, "is as follow")
            ''' print the inbound link of nodes '''
            if(node in self.inbound_links.keys()):
                print("inbound connection: ", self.inbound_links[node])
            ''' print the outbound link of nodes '''
            if(node in self.outbound_links.keys()):
                print("outbound connection: ", self.outbound_links[node])
            print()
            print()
    ''' retrive the maximum connection for nodes in our graph '''

    def max_connection(self):
        current_max = 0
        for node in self.nodes:
            ''' initially set the connection of node to 0 '''
            tot_conn = 0
            ''' if node has inbound links then count the length of
            list containing inbound links '''
            if(node in self.inbound_links.keys()):
                tot_conn = tot_conn + len(self.inbound_links[node])
            ''' if node has outbound links then count the length of
            list containing outbound links '''
            if(node in self.outbound_links.keys()):
                tot_conn = tot_conn + len(self.outbound_links[node])
            if(current_max <= tot_conn):
                current_max = tot_conn
        return current_max

    ''' this method parse the graph from string '''

    def parse_graph(self, input_string):
        ''' split the string on ->  '''
        input_string = input_string.split("->")
        for i in range(0, len(input_string) - 1):
            ''' add edges in the graph '''
            self.add_edge(int(input_string[i]), int(input_string[i + 1]))


''' this method identify the the node with max connections and complexity of
this function is O(n) '''


def identify_router(graph):
    ''' first retrive what is the max connection in current graph '''
    max_conn = graph.max_connection()
    set_of_node = []
    ''' iterate through all nodes '''
    for node in graph.nodes:
        ''' initially set the connection of node to 0 '''
        tot_conn = 0
        ''' if node has inbound links then count the length of
            list containing inbound links '''
        if(node in graph.inbound_links.keys()):
            tot_conn = tot_conn + len(graph.inbound_links[node])
        ''' if node has outbound links then count the length of
            list containing outbound links '''
        if(node in graph.outbound_links.keys()):
            tot_conn = tot_conn + len(graph.outbound_links[node])
        ''' if node is having max connection then add it to set of node'''
        if(tot_conn == max_conn):
            set_of_node.append(node)
    ''' return the result concated by ,'''
    return ",".join(set_of_node)


# graph = Graph("1 -> 3 -> 5 -> 6 -> 4 -> 5 -> 2 -> 6")
# print(identify_router(graph))
