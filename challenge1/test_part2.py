from part2 import Graph, identify_router

try:
    g1 = Graph("1 -> 2 -> 3 -> 5 -> 2 -> 1")
    assert identify_router(g1) == "2", "Test failed for input \
        1 -> 2 -> 3 -> 5 -> 2 -> 1"
    g2 = Graph("1 -> 3 -> 5 -> 6 -> 4 -> 5 -> 2 -> 6")
    assert identify_router(g2) == "5", "Test failed for input \
        1 -> 3 -> 5 -> 6 -> 4 -> 5 -> 2 -> 6"
    g3 = Graph("2 -> 4 -> 6 -> 2 -> 5 -> 6 ")
    assert identify_router(g3) == "2,6", "Test failed for input \
        2 -> 4 -> 6 -> 2 -> 5 -> 6 "
    print("all test passed")
except AssertionError as e:
    print(e)
