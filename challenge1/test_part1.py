from part1 import compress

try:
    assert compress("bbcceeee") == "b2c2e4", "Test failed for input bbcceeee"
    assert compress(
        "aaabbbcccaaa") == "a3b3c3a3", "Test failed for input aaabbbcccaa"
    assert compress("a") == "a", "Test failed for input a"
    print("all test passed")
except AssertionError as e:
    print(e)
