# challenge 1  : Algorithm test


''' complexity of this function is O(n) '''


def compress(input_string):
    ''' output string to store the result '''
    output_string = ""
    ''' this keep track of which previous char we are processing '''
    current_char = ""
    ''' this keep track of how many instance are there for currently
    processing character '''
    current_count = 0
    ''' loop through the input string '''
    for i in input_string:
        ''' if previous character is not same as the current one '''
        if(i != current_char):
            ''' if current count is not zero then append result to output
            string '''
            if(current_count != 0):
                ''' if current count is not 1 then only we have to show the
                number '''
                if(current_count != 1):
                    output_string = \
                        output_string + current_char + str(current_count)
                else:
                    output_string = \
                        output_string + current_char
            ''' update the current_char to the new char whihc is different then
            the previous one and set it's count to 1 '''
            current_char = i
            current_count = 1
        else:
            ''' if previous character is same as current ont then just add
        one to cout and proceed further '''
            current_count = current_count + 1
    ''' after coming out of loop we have to add the last character we
    were processing because that last character was not processed
    according condition we added '''
    if(current_count != 1):
        output_string = \
            output_string + current_char + str(current_count)
    else:
        output_string = \
            output_string + current_char
    return output_string
